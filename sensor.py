import RPi.GPIO as GPIO
import time
from datetime import datetime

pin = 40

GPIO.setmode(GPIO.BOARD)
GPIO.setup(pin, GPIO.IN)
pulsos_por_minuto = 0
constante = 0.0000062
tempo_novo = 0.0

litrospormin = 0

while (True):
    tempo_novo = time.time() + 60
    pulsos_por_minuto = 0
    while time.time() <= tempo_novo:
        if(GPIO.input(pin) != 0):
            pulsos_por_minuto += 1
    litrospormin = round(pulsos_por_minuto * constante, 2)

    timestamp = int(datetime.timestamp(datetime.now()))
    f = open("log.txt", "a+")
    f.write(str(litrospormin) + ' ' + str(timestamp) + '\n')
    f.close()
    print("Litros por minuto", litrospormin)
    print("Litros por hora", litrospormin * 60)
